<?php

namespace App\Entity;

use App\Repository\PatientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PatientRepository::class)
 */
class Patient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="integer")
     */
    private $age;

    /**
     * @ORM\Column(type="string", length=800)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tel;

    /**
     * @ORM\Column(type="date")
     */
    private $entree;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $sortie;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $chirurgien;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $aide;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $diagnostique;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $malade = [];

    /**
     * @ORM\Column(type="json")
     */
    private $gardeMalade = [];

    /**
     * @ORM\Column(type="json")
     */
    private $medicament = [];

    /**
     * @ORM\Column(type="json")
     */
    private $suture = [];

    /**
     * @ORM\Column(type="json")
     */
    private $soins = [];

    /**
     * @ORM\Column(type="json")
     */
    private $bilanSanguin = [];

    /**
     * @ORM\Column(type="json")
     */
    private $radiologie = [];


    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $bloc = [];

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getEntree(): ?\DateTimeInterface
    {
        return $this->entree;
    }

    public function setEntree(\DateTimeInterface $entree): self
    {
        $this->entree = $entree;

        return $this;
    }

    public function getSortie(): ?\DateTimeInterface
    {
        return $this->sortie;
    }

    public function setSortie(?\DateTimeInterface $sortie): self
    {
        $this->sortie = $sortie;

        return $this;
    }

    public function getChirurgien(): ?string
    {
        return $this->chirurgien;
    }

    public function setChirurgien(string $chirurgien): self
    {
        $this->chirurgien = $chirurgien;

        return $this;
    }

    public function getAide(): ?string
    {
        return $this->aide;
    }

    public function setAide(string $aide): self
    {
        $this->aide = $aide;

        return $this;
    }

    public function getDiagnostique(): ?string
    {
        return $this->diagnostique;
    }

    public function setDiagnostique(string $diagnostique): self
    {
        $this->diagnostique = $diagnostique;

        return $this;
    }

    public function getMalade(): ?array
    {
        return $this->malade;
    }

    public function setMalade(?array $malade): self
    {
        $this->malade = $malade;

        return $this;
    }

    public function getGardeMalade(): ?array
    {
        return $this->gardeMalade;
    }

    public function setGardeMalade(array $gardeMalade): self
    {
        $this->gardeMalade = $gardeMalade;

        return $this;
    }

    public function getMedicament(): ?array
    {
        return $this->medicament;
    }

    public function setMedicament(array $medicament): self
    {
        $this->medicament = $medicament;

        return $this;
    }

    public function getSuture(): ?array
    {
        return $this->suture;
    }

    public function setSuture(array $suture): self
    {
        $this->suture = $suture;

        return $this;
    }

    public function getSoins(): ?array
    {
        return $this->soins;
    }

    public function setSoins(array $soins): self
    {
        $this->soins = $soins;

        return $this;
    }

    public function getBilanSanguin(): ?array
    {
        return $this->bilanSanguin;
    }

    public function setBilanSanguin(array $bilanSanguin): self
    {
        $this->bilanSanguin = $bilanSanguin;

        return $this;
    }

    public function getRadiologie(): ?array
    {
        return $this->radiologie;
    }

    public function setRadiologie(array $radiologie): self
    {
        $this->radiologie = $radiologie;

        return $this;
    }


    public function getBloc(): ?array
    {
        return $this->bloc;
    }

    public function setBloc(?array $bloc): self
    {
        $this->bloc = $bloc;

        return $this;
    }
}
