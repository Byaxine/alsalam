<?php

namespace App\Form;

use App\Entity\Patient;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PatientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // User 
            ->add('nom', TextType::class, [
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Nom']
            ])
            ->add('prenom', TextType::class, [
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Prénom']
            ])
            ->add('age', NumberType::class, [
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Age']
            ])
            ->add('adresse', TextType::class, [
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Adresse']
            ])
            ->add('tel', TextType::class, [
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Téléphone']
            ])
            ->add('chirurgien', TextType::class, [
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Chirurgien']
            ])
            ->add('entree', DateType::class, [
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Entrée', 'type' => 'datetime-local']
            ])
            ->add('sortie', null, [
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Sortie']
            ])
            ->add('aide', TextType::class, [
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Aide']
            ])
            ->add('diagnostique', TextType::class, [
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Diagnostique']
            ])
            // end user
            // Bloc Operation
            ->add('location', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Location']
            ])
            ->add('supp_ins', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Supp Instrumentation']
            ])
            ->add('supp_ane', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Supp anesthésie']
            ])
            ->add('total_bloc', NumberType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Total']
            ])
            // End Bloc Operation
            // Hospitalisation
            ->add('malade_tarif', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            ->add('malade_jour', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            ->add('malade_total', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])

            ->add('garde_malade_tarif', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            ->add('garde_malade_jour', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            ->add('garde_malade_total', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            // End Hospitalisation
            // Soins et Medicament
            // Medicament
            ->add('med1', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            ->add('med2', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            ->add('med3', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            ->add('med4', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            // End Medicament
            // Files de suture
            ->add('suture1', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            ->add('suture2', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            ->add('suture3', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            ->add('suture4', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])

            // End Files de suture
            // Soins particuliers 
            ->add('soins1', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            ->add('soins2', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            ->add('soins3', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            ->add('soins4', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            // End Soins particuliers 
            // Bilan sanguin
            ->add('bilan1', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            ->add('bilan2', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            ->add('bilan3', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            ->add('bilan4', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            // End Bilan sanguin
            // Radiologie
            ->add('radio1', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            ->add('radio2', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            ->add('radio3', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ])
            ->add('radio4', TextType::class, [
                'required' => false,
                'mapped' => false,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => '']
            ]);
        // End Radiologie
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Patient::class,
        ]);
    }
}
