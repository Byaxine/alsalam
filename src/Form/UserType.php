<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Nom']
            ])
            ->add(
                'prenom',
                TextType::class,
                [
                    'label' => false,
                    'attr' => ['class' => 'form-control', 'placeholder' => 'Prénom']
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => false,
                    'attr' => ['class' => 'form-control', 'placeholder' => 'Email']
                ]
            )
            ->add('roles', ChoiceType::class, [
                'label' => false,
                'attr' => ['class' => 'form-control'],
                'choices' =>
                array(
                    'Administrateur' => "ROLE_ADMIN",
                    'Utilisateur'  => "ROLE_USER"
                ),
                'multiple' => true,
                'required' => true,
            ])
            ->add('plainPassword', PasswordType::class, [
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Mot de passe'],
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
