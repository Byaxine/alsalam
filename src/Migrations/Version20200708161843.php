<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200708161843 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE bloc (id INT AUTO_INCREMENT NOT NULL, patient_id INT NOT NULL, location VARCHAR(255) NOT NULL, instrumentation VARCHAR(255) NOT NULL, anesthesie VARCHAR(255) NOT NULL, total INT NOT NULL, INDEX IDX_C778955A6B899279 (patient_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE historique (id INT AUTO_INCREMENT NOT NULL, data LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', created_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE patient (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, age INT NOT NULL, adresse VARCHAR(800) NOT NULL, tel VARCHAR(255) NOT NULL, entree DATE NOT NULL, sortie DATE DEFAULT NULL, chirurgien VARCHAR(255) NOT NULL, aide VARCHAR(255) NOT NULL, diagnostique VARCHAR(1000) NOT NULL, malade JSON DEFAULT NULL, garde_malade JSON NOT NULL, medicament JSON NOT NULL, suture JSON NOT NULL, bilan_sanguin JSON NOT NULL, radiologie JSON NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bloc ADD CONSTRAINT FK_C778955A6B899279 FOREIGN KEY (patient_id) REFERENCES patient (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bloc DROP FOREIGN KEY FK_C778955A6B899279');
        $this->addSql('DROP TABLE bloc');
        $this->addSql('DROP TABLE historique');
        $this->addSql('DROP TABLE patient');
    }
}
