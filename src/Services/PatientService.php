<?php

namespace App\Services;

use DateTime;
use DateTimeZone;
use App\Entity\Bloc;
use App\Entity\Patient;
use App\Entity\Historique;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class PatientService
{
    public function test(array $var, Patient $patient)
    {
        $patient->setMalade(array(
            "tarif" => $var['malade_tarif'],
            "jour" =>  $var['malade_jour'],
            "total" =>  $var['malade_total']
        ));
        $patient->setGardeMalade(array(
            "tarif" => $var['garde_malade_tarif'],
            "jour" =>  $var['garde_malade_jour'],
            "total" =>  $var['garde_malade_total']
        ));
        $patient->setMedicament(array(
            "qte" => $var['med1'],
            "prixUnitaire" =>  $var['med2'],
            "qte1" =>  $var['med3'],
            "total" =>  $var['med4']
        ));
        $patient->setSuture(array(
            "qte" => $var['suture1'],
            "prixUnitaire" =>  $var['suture2'],
            "qte1" =>  $var['suture3'],
            "total" =>  $var['suture4']
        ));
        $patient->setSoins(array(
            "qte" => $var['soins1'],
            "prixUnitaire" =>  $var['soins2'],
            "qte1" =>  $var['soins3'],
            "total" =>  $var['soins4']
        ));
        $patient->setBilanSanguin(array(
            "qte" => $var['bilan1'],
            "prixUnitaire" =>  $var['bilan2'],
            "qte1" =>  $var['bilan3'],
            "total" =>  $var['bilan4']
        ));
        $patient->setRadiologie(array(
            "qte" => $var['radio1'],
            "prixUnitaire" =>  $var['radio2'],
            "qte1" =>  $var['radio3'],
            "total" =>  $var['radio4']
        ));
        $patient->setBloc(array(
            "location" => $var['location'],
            "supp_ins" =>  $var['supp_ins'],
            "supp_ane" =>  $var['supp_ane'],
            "total_bloc" =>  $var['total_bloc']
        ));

        // dd($patient);

        return $patient;
    }

    public function editForm(FormInterface $form, Patient $patient)
    {


        $bloc = $patient->getBloc();
        $form->get('location')->setData($bloc["location"]);
        $form->get('supp_ins')->setData($bloc["supp_ins"]);
        $form->get('supp_ane')->setData($bloc["supp_ane"]);
        $form->get('total_bloc')->setData(intval($bloc["total_bloc"]));
        dump($bloc);
        $malade = $patient->getMalade();
        $form->get('malade_tarif')->setData($malade["tarif"]);
        $form->get('malade_jour')->setData($malade["jour"]);
        $form->get('malade_total')->setData(intval($malade["total"]));

        $gardeMalade = $patient->getGardeMalade();
        $form->get('garde_malade_tarif')->setData($gardeMalade["tarif"]);
        $form->get('garde_malade_jour')->setData($gardeMalade["jour"]);
        $form->get('garde_malade_total')->setData(intval($gardeMalade["total"]));

        $medicament = $patient->getMedicament();
        $form->get('med1')->setData($medicament["qte"]);
        $form->get('med2')->setData($medicament["prixUnitaire"]);
        $form->get('med3')->setData($medicament["qte1"]);
        $form->get('med4')->setData(intval($medicament["total"]));
        dump($medicament);


        $suture = $patient->getSuture();
        $form->get('suture1')->setData($suture["qte"]);
        $form->get('suture2')->setData($suture["prixUnitaire"]);
        $form->get('suture3')->setData($suture["qte1"]);
        $form->get('suture4')->setData(intval($suture["total"]));
        dump($medicament);

        $soins = $patient->getSoins();
        $form->get('soins1')->setData($soins["qte"]);
        $form->get('soins2')->setData($soins["prixUnitaire"]);
        $form->get('soins3')->setData($soins["qte1"]);
        $form->get('soins4')->setData(intval($soins["total"]));
        dump($soins);

        $bilan = $patient->getBilanSanguin();
        $form->get('bilan1')->setData($bilan["qte"]);
        $form->get('bilan2')->setData($bilan["prixUnitaire"]);
        $form->get('bilan3')->setData($bilan["qte1"]);
        $form->get('bilan4')->setData(intval($bilan["total"]));
        dump($bilan);

        $radio = $patient->getRadiologie();
        $form->get('radio1')->setData($radio["qte"]);
        $form->get('radio2')->setData($radio["prixUnitaire"]);
        $form->get('radio3')->setData($radio["qte1"]);
        $form->get('radio4')->setData(intval($radio["total"]));
        dump($radio);
        return $form;
    }

    public function archivePatient(Patient $patient)
    {
        $historique = new Historique();
        $historique->setData($patient);
        $historique->setCreatedAt(new DateTime("now", new DateTimeZone('Africa/Algiers')));

        return $historique;
    }
}
